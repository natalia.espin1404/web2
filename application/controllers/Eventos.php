<?php

class Eventos extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('evento');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('eventos/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['eventos']=$this->evento->obtenerTodos();
    $this->load->view('header');
    $this->load->view('eventos/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoEvento=array(
      "nombre_eve"=>$this->input->post('nombre_eve'),
      "lugar_eve"=>$this->input->post('lugar_eve'),
      "total_personas_eve"=>$this->input->post('total_personas_eve'),
      "comida_eve"=>$this->input->post('comida_eve'),
      "tipo_evento_eve"=>$this->input->post('tipo_evento_eve')
    );
    if($this->evento->insertar($datosNuevoEvento)){
      redirect('eventos/index');

    }else {
      echo "<h1>ERRORE AL INSERTAR </h1>";
    }

  }
  //funcion para Eliminar eventos
  //metodo get
  public function eliminar($id_eve){
   if ($this->evento->borrar($id_eve)) { //invocando al modelo
     redirect('clientes/index');
   } else {
     echo "ERROR AL BORRAR :(";
   }

  }

}//NO borrar el cierre de la clase


 ?>
