<?php

class Clientes extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('cliente');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('clientes/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['clientes']=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('clientes/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoCliente=array(
      "nombre_cli"=>$this->input->post('nombre_cli'),
      "apellido_cli"=>$this->input->post('apellido_cli'),
      "telefono_cli"=>$this->input->post('telefono_cli'),
      "numero_personas_cli"=>$this->input->post('numero_personas_cli'),
      "edad_cli"=>$this->input->post('edad_cli')
    );
    if($this->cliente->insertar($datosNuevoCliente)){
      redirect('clientes/index');

    }else {
      echo "<h1>ERRORE AL INSERTAR </h1>";
    }

  }
  //funcion para Eliminar Clientes
  //metodo get
  public function eliminar($id_cli){
   if ($this->cliente->borrar($id_cli)) { //invocando al modelo
     redirect('clientes/index');
   } else {
     echo "ERROR AL BORRAR :(";
   }

  }

}//NO borrar el cierre de la clase


 ?>
