<?php

class Recepciones extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('recepcion');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('recepciones/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['recepciones']=$this->recepcion->obtenerTodos();
    $this->load->view('header');
    $this->load->view('recepciones/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoRecepcion=array(
      "nombre_re"=>$this->input->post('nombre_re'),
      "decoracion_re"=>$this->input->post('decoracion_re'),
      "direccion_re"=>$this->input->post('direccion_re'),
      "tamaño_re"=>$this->input->post('tamaño_re'),
      "empleados_re"=>$this->input->post('empleados_re')
    );
    if($this->recepcion->insertar($datosNuevoRecepcion)){
      redirect('recepciones/index');

    }else {
      echo "<h1>ERRORE AL INSERTAR </h1>";
    }

  }
  //funcion para Eliminar eventos
  //metodo get
  public function eliminar($id_re){
   if ($this->recepcion->borrar($id_re)) { //invocando al modelo
     redirect('recepciones/index');
   } else {
     echo "ERROR AL BORRAR :(";
   }

  }

}//NO borrar el cierre de la clase


 ?>
