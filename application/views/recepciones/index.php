<h1>Listado de Recepciones</h1>
<br>
<?php if ($recepciones): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th> DECORACION</th>
        <th>  DIRECCION</th>
        <th> TAMAÑO</th>
        <th>EMPLEADOS</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($recepciones as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_re ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_re ?>
          </td>
          <td>
            <?php echo $filaTemporal->decoracion_re ?>
          </td>
          <td>
            <?php echo $filaTemporal->direccion_re ?>
          </td>
          <td>
            <?php echo $filaTemporal->tamaño_re ?>
          </td>
          <td>
            <?php echo $filaTemporal->empleados_re ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Recepcion"
            style="color:blue;">
            <button type="submit" name="button" class="btn btn-primary">
              <i class="glyphicon glyphicon-pencil"></i>
              </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/recepciones/eliminar/<?php echo $filaTemporal->id_re; ?>" title="Eliminar Recepcion"
            style="color:red;">
            <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
            </button>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1>No hay Recepciones</h1>
<?php endif; ?>
