<h1>NUEVA RECEPCION</h1>
<form class=""
action="<?php echo site_url(); ?>/recepciones/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_re" value=""
          id="nombre_re">
      </div>
      <div class="col-md-4">
          <label for=""> Decoracion:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el tipo de decoracion"
          class="form-control"
          name="decoracion_re" value=""
          id="decoracion_re">
      </div>
      <div class="col-md-4">
        <label for="">Direccion:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la direccion"
        class="form-control"
        name="direccion_re" value=""
        id="direccion_re">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Tamaño:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el tamaño"
          class="form-control"
          name="tamaño_re" value=""
          id="tamaño_re">
      </div>
      <div class="col-md-4">
          <label for="">Empleados:</label>
          <br>
          <input type="number"
          placeholder="Ingrese el numero de empleados que desea"
          class="form-control"
          name="empleados_re" value=""
          id="empleados_re">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/recepciones/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
