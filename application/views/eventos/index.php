<h1>Listado de Eventos</h1>
<br>
<?php if ($eventos): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th> LUGAR</th>
        <th> TOTAL DE PERSONAS</th>
        <th> COMIDA</th>
        <th>TIPO DE EVENTO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($eventos as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_eve ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_eve ?>
          </td>
          <td>
            <?php echo $filaTemporal->lugar_eve ?>
          </td>
          <td>
            <?php echo $filaTemporal->total_personas_eve ?>
          </td>
          <td>
            <?php echo $filaTemporal->comida_eve ?>
          </td>
          <td>
            <?php echo $filaTemporal->tipo_evento_eve ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Evento"
            style="color:blue;">
            <button type="submit" name="button" class="btn btn-primary">
              <i class="glyphicon glyphicon-pencil"></i>
              </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/eventos/eliminar/<?php echo $filaTemporal->id_eve; ?>" title="Eliminar Evento"
            style="color:red;">
            <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
              </button>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1>No hay Eventos</h1>
<?php endif; ?>
