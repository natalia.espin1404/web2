<h1>NUEVO EVENTO</h1>
<form class=""
action="<?php echo site_url(); ?>/eventos/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_eve" value=""
          id="nombre_eve">
      </div>
      <div class="col-md-4">
          <label for=""> Lugar:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el  Lugar"
          class="form-control"
          name="lugar_eve" value=""
          id="lugar_eve">
      </div>
      <div class="col-md-4">
        <label for="">Total de personas:</label>
        <br>
        <input type="number"
        placeholder="Ingrese el total de personas"
        class="form-control"
        name="total_personas_eve" value=""
        id="total_personas_eve">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Comida:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el tipo de comida"
          class="form-control"
          name="comida_eve" value=""
          id="comida_eve">
      </div>
      <div class="col-md-4">
          <label for="">Tipo de Evento:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el tipo de Evento"
          class="form-control"
          name="tipo_evento_eve" value=""
          id="tipo_evento_eve">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/eventos/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
