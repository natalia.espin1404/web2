<h1>NUEVO CLIENTE</h1>
<form class=""
action="<?php echo site_url(); ?>/clientes/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_cli" value=""
          id="nombre_cli">
      </div>
      <div class="col-md-4">
          <label for=""> Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el  apellido"
          class="form-control"
          name="apellido_cli" value=""
          id="apellido_cli">
      </div>
      <div class="col-md-4">
        <label for="">Telefono:</label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_cli" value=""
        id="telefono_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Numero de Personas:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el numero de personas"
          class="form-control"
          name="numero_personas_cli" value=""
          id="numero_personas_cli">
      </div>
      <div class="col-md-4">
          <label for="">Edad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la edad"
          class="form-control"
          name="edad_cli" value=""
          id="edad_cli">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
