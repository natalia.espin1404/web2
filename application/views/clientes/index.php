<h1>Listado de Clientes</h1>
<br>
<?php if ($clientes): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th> APELLIDO</th>
        <th> TELEFONO</th>
        <th>NUMERO DE PERSONAS</th>
        <th>EDAD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_cli ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_cli ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_cli ?>
          </td>
          <td>
            <?php echo $filaTemporal->telefono_cli ?>
          </td>
          <td>
            <?php echo $filaTemporal->numero_personas_cli ?>
          </td>
          <td>
            <?php echo $filaTemporal->edad_cli ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Cliente"
            style="color:blue;">
            <button type="submit" name="button" class="btn btn-primary">
              <i class="glyphicon glyphicon-pencil"></i>
              </button>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>" title="Eliminar Cliente"
            style="color:red;">
            <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
              </button>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1>No hay Clientes</h1>
<?php endif; ?>
