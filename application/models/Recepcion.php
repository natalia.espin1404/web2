<?php
  class Recepcion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un cliente en MYSQL
    function insertar($datos){
        return $this->db->insert("recepciones",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoRecepciones=
      $this->db->get("recepciones");
      if($listadoRecepciones->num_rows()>0){ //si hay datos
      return $listadoRecepciones->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_re){
      $this->db->where("id_re",$id_re);
      if ($this->db->delete("recepciones")) {
        return true;
      } else {
        return false;
      }

    }
  }//Cierre de la clase

 ?>
