<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un cliente en MYSQL
    function insertar($datos){
        return $this->db->insert("clientes",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoClientes=
      $this->db->get("clientes");
      if($listadoClientes->num_rows()>0){ //si hay datos
      return $listadoClientes->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_cli){
      $this->db->where("id_cli",$id_cli);
      if ($this->db->delete("clientes")) {
        return true;
      } else {
        return false;
      }

    }
  }//Cierre de la clase

 ?>
