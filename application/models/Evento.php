<?php
  class Evento extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un cliente en MYSQL
    function insertar($datos){
        return $this->db->insert("eventos",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoEventos=
      $this->db->get("eventos");
      if($listadoEventos->num_rows()>0){ //si hay datos
      return $listadoEventos->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_eve){
      $this->db->where("id_eve",$id_eve);
      if ($this->db->delete("eventos")) {
        return true;
      } else {
        return false;
      }

    }
  }//Cierre de la clase

 ?>
